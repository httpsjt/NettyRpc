package com.tjs.test;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.tjs.netty.annotation.RemoteInvoke;
import com.tjs.netty.client.NettyClient;
import com.tjs.netty.param.ClientRequest;
import com.tjs.netty.param.Response;
import com.tjs.user.UserRemote;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=RemoteInvokeTest.class)
@ComponentScan("com.tjs")
public class RemoteInvokeTest  {
	
	@RemoteInvoke
	private UserRemote userRemote;
	
	@Test
	public void findUser() throws InterruptedException {
		Response findUser = userRemote.findUser("tjs");
		System.out.println("test findUser " + findUser);
		Thread.sleep(1000000000);
	}
	
	@Test
	public void test() {
		ClientRequest request = new ClientRequest();
		request.setContent("客户端第一次发送的数据");
		request.setCommand("com.tjs.user.controller.UserController.test01");
		Response send = NettyClient.send(request);
		System.out.println("test " + send);
	}
	
	@Test
	public void test2() {
		ClientRequest request = new ClientRequest();
		request.setContent("查询用户名称");
		request.setCommand("com.tjs.user.controller.UserController.findUser");
		Response send = NettyClient.send(request);
		System.out.println("test " + send);
	}

}
