package com.tjs.test;


import org.junit.Test;

import com.tjs.netty.client.NettyClient;
import com.tjs.netty.param.ClientRequest;
import com.tjs.netty.param.Response;

public class TestSendMsg {

	@Test
	public void test() {
		ClientRequest request = new ClientRequest();
		request.setContent("客户端第一次发送的数据");
		request.setCommand("com.tjs.user.controller.UserController.test01");
		Response send = NettyClient.send(request);
		System.out.println("test " + send);
	}
	
	@Test
	public void test2() {
		ClientRequest request = new ClientRequest();
		request.setContent("查询用户名称");
		request.setCommand("com.tjs.user.controller.UserRemoteImpl.findUser");
		Response send = NettyClient.send(request);
		System.out.println("test " + send);
	}

}
