package com.tjs.user;

import com.tjs.netty.param.Response;
import com.tjs.user.bean.User;

public interface UserRemote {
	
	public Response saveUser(User user);
	public Response findUser(String name);
}

