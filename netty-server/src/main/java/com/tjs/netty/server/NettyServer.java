package com.tjs.netty.server;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.tjs")
public class NettyServer {
	
	//多看看NettyInital
	@SuppressWarnings({"resource","unused"})
	public static void main(String[] args) throws Exception {
		ApplicationContext context = new AnnotationConfigApplicationContext(NettyServer.class); 
		
	}
}
