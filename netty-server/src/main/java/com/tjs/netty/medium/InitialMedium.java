package com.tjs.netty.medium;

import java.lang.reflect.Method;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;

import com.tjs.netty.annotation.Remote;

@Component
public class InitialMedium implements BeanPostProcessor {

	@Override
	public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
		if (bean.getClass().isAnnotationPresent(Remote.class)) {
			// 获取它实现的第一个接口的所有方法名称
			Method[] methods = bean.getClass().getInterfaces()[0].getDeclaredMethods();
			for (Method method : methods) {
				String key = bean.getClass().getInterfaces()[0].getName() + "." + method.getName();
				//String key = bean.getClass().getName() + "." + method.getName();
				System.out.println(key);
				BeanMethod beanMethod = new BeanMethod(method, bean);
				Media.beanMap.put(key, beanMethod);
			}
		}
		return bean;
	}

	@Override
	public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
		return bean;
	}

}
