package com.tjs.user.controller;

import org.springframework.beans.factory.annotation.Autowired;

import com.tjs.netty.annotation.Remote;
import com.tjs.netty.param.Response;
import com.tjs.user.UserRemote;
import com.tjs.user.bean.User;
import com.tjs.user.service.UserService;

@Remote
public class UserRemoteImpl implements UserRemote {
	
	@Autowired
	private UserService userService;

	@Override
	public Response saveUser(User user) {
		userService.saveUser(user);
		return Response.createSuccessResult(user);
	}

	@Override
	public Response findUser(String name) {
		User user = userService.findUser(name);
		return Response.createSuccessResult(user);
	}
	
}
