package com.tjs.netty.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.stereotype.Component;

/**
 * 
* @ClassName: Remote
* @Description: 给服务端编程用的
* @ClassName: Remote
* @<p>Company: www.***.com</p>
* @author @author taojinsen
* @date 2018年5月16日 上午8:25:37
* @version 1.0
 */
@Target({ElementType.TYPE,ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Component
public @interface Remote {
	
	String value() default "";
	
}	
