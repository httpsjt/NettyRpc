package com.tjs.netty.param;

public class Response {

	private Long id;
	private Object result; // 响应结果
	
	private String code = "0000"; //00000 成功，其他表示失败
	private String msg ; //失败原因

	public Response() {
	}

	public Response(Long id, Object result) {
		super();
		this.id = id;
		this.result = result;
	}

	
	public static Response createSuccessResult() {
		return new Response();
	}
	
	public static Response createSuccessResult(Object content) {
		 Response response = new Response();
		 response.setResult(content);
		 return response;
	}
	public static Response createFailResult(String code, String msg) {
		 Response response = new Response();
		 response.setMsg(msg);
		 response.setCode(code);
		 return response;
	}

	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Object getResult() {
		return result;
	}

	public void setResult(Object result) {
		this.result = result;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	@Override
	public String toString() {
		return "Response [id=" + id + ", result=" + result + ", code=" + code + ", msg=" + msg + "]";
	}
}
